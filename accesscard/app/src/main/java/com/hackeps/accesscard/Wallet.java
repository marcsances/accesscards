package com.hackeps.accesscard;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * Created by Marc Sances on 19/11/2017.
 */

public class Wallet {
    private static final String PREF_ACCOUNT_NUMBER = "account_number";
    private static final String DEFAULT_PERSON_ID = "94563287";
    private static final String TAG = "Wallet";
    private static String personId = null;
    private static final Object sAccountLock = new Object();

    public static void setPersonId(Context c, String s) {
        synchronized(sAccountLock) {
            Log.i(TAG, "Setting person id: " + s);
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
            prefs.edit().putString(PREF_ACCOUNT_NUMBER, s).commit();
            personId = s;
        }
    }

    public static String getPersonId(Context c) {
        synchronized (sAccountLock) {
            if (personId == null) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
                String pid = prefs.getString(PREF_ACCOUNT_NUMBER, DEFAULT_PERSON_ID);
                personId = pid;
            }
            return personId;
        }
    }
}
